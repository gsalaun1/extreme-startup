package gsalaun1.extremestartup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExtremestartupApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExtremestartupApplication.class, args);
	}

}
