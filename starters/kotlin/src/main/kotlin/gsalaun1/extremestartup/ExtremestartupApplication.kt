package gsalaun1.extremestartup

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ExtremestartupApplication

fun main(args: Array<String>) {
    runApplication<ExtremestartupApplication>(*args)
}
