# Extreme Startup

## Serveur

https://github.com/rchatley/extreme_startup

Récupérer l'image Docker
```shell script
docker pull tclavier/extreme_startup
```

Warmup
```shell script
docker run -d -p 80:3000 -e WARMUP=1 tclavier/extreme_startup   # ./warmup.sh
```

Lancer le serveur
```shell script
docker run -d -p 80:3000 tclavier/extreme_startup  # ./run.sh
```

Leaderboard : http://localhost/graph

Players : http://localhost/players

Control panel : http://localhost/controlpanel

## Infos

Article Xebia (2012) : https://blog.engineering.publicissapient.fr/2012/07/19/extreme-startup-chez-xebia/

Starters : https://github.com/sopra-steria-norge/extreme_startup_servers

