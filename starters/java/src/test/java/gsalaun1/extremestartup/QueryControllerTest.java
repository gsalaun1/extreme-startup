package gsalaun1.extremestartup;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class QueryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_query() throws Exception {
        mockMvc.perform(get("/").param("q","foo"))
                .andExpect(status().isOk())
                .andExpect(content().string("foo"));
    }
}
