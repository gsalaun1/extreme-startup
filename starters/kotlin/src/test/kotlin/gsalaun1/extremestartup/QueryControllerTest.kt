package gsalaun1.extremestartup

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@WebMvcTest
class QueryControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    fun `Should return query`() {
        mockMvc.get("/") {
            param("q", "foo")
        }
            .andExpect {
                status { isOk() }
                content { string("foo") }
            }
    }
}
